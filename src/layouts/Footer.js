import React from 'react'
import footerLogo from '../assets/footerLogo.png'
function Footer(props) {
    return (
        <div className='flex w-full py-32 bg-footerBg bg-cover bg-no-repeat sm:bg-none justify-center sm:py-20 md:py-16'>
            <div className='flex justify-center w-11/12 flex-col items-center'>
                <img src={footerLogo} alt='footer logo' className='sm:w-3/6 md:w-20' />
                <ul className='flex sm:flex-col sm:items-center sm:justify-center mt-10 justify-between gap-x-5 md:text-xs-custom text-xl md:mt-3'>
                    <li onClick={props.discoverClick}>DISCOVER OQULO</li>
                    <li onClick={props.featureClick}>FEATURE</li>
                    <li onClick={props.contactClick}>CONTACT</li>
                </ul>
                <p className='mt-16 md:text-[.3rem] md:mt-3 text-center sm:text-[.8rem]'>Copyright © Oqulo 2018. All rights reserved.</p>
            </div>
        </div>
    )
}

export default Footer