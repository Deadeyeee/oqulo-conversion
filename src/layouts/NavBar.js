import React, { useState } from 'react'
import Phone from '../assets/iPhone7.png'
import logo from '../assets/logo.png'
import Button from '../components/ui/Button'
import Input from '../components/ui/Input'
function NavBar(props) {
  const [isActive, setIsActive] = useState(false);
  return (
    <div className='flex flex-col sm:overflow-hidden bg-nav-bg bg-cover bg-no-repeat w-full pt-5 sm:pt-8 overflow-hidden text-white items-center sm:bg-gradient-to-br from-blue to-pink'>
      {isActive && <div className='hidden overflow-y-hidden flex-col items-center justify-center sm:flex fixed inset-0 w-screen h-screen bg-gradient-to-br from-blue to-pink z-20'>
        <div
          className='inline-block w-6 h-6 absolute top-14 right-10'
          onClick={() => {
            setIsActive(!isActive);
          }}
        >
          <span className='block absolute bg-white top-0 left-0 w-full h-[.1rem] border-[2px_solid_white] rotate-45'></span>
          <span className='block absolute bg-white top-0 left-0 w-full h-[.1rem] border-[2px_solid_white] rotate-[-45deg]'></span>
        </div>
        <img src={logo} alt="logo" className='sm:w-2/6 mb-20' />
        <ul className='flex w-full flex-col justify-center items-center gap-y-8 md:text-xs-custom text-xl'>
          <li onClick={() => {
            setIsActive(!isActive)
          }}>DISCOVER OQULO</li>
          <li onClick={() => {
            setIsActive(!isActive)
          }}>FEATURE</li>
          <li onClick={() => {
            setIsActive(!isActive)
          }}>CONTACT</li>
        </ul>
      </div>}

      <div className='flex justify-between items-center w-3/4 sm:w-11/12' >
        <img src={logo} alt="logo" className='sm:w-2/6 md:w-20' />
        <ul className='flex sm:hidden justify-between gap-x-5 md:text-xs-custom text-xl'>
          <li onClick={props.discoverClick}>DISCOVER OQULO</li>
          <li onClick={props.featureClick}>FEATURE</li>
          <li onClick={props.contactClick}>CONTACT</li>
        </ul>
        <div className='hidden sm:flex flex-col gap-1'
          onClick={() => {
            setIsActive(!isActive);
          }}>
          <span className='bg-white w-5 h-0.5' />
          <span className='bg-white w-5 h-0.5' />
          <span className='bg-white w-5 h-0.5' />
        </div>

      </div>
      <div className='flex justify-center mt-10 md:mt-2 sm:mt-10 md:gap-10 gap-20 w-3/4 sm:flex-col sm:w-11/12'>
        <div className='flex justify-end sm:justify-center w-1/2 sm:order-2 sm:w-full'>
          <img src={Phone} className='md:w-3/4 sm:w-3/4 w-[80%] ' alt="phone" style={{ filter: "drop-shadow(0px 0px 20px rgb(0,0,0, 0.3))" }} />
        </div>
        <div className='flex flex-col w-fill w-1/2 sm:w-full sm:order-1'>
          <h1 className='font-semibold mt-2 text-[2.8rem] md:text-lg sm:text-2xl leading-[3rem]'>
            The Only Platform You’ll Need to Run Smart Coworking Spaces & Serviced Offices
          </h1>
          <p className='md:text-xs-custom text-[1.35rem] sm:text-base mt-12 sm:mt-5 md:mt-2 md:leading-3'>
            Oqulo is built to sell, manage and grow your commercial real estate business.
            Collect payments, manage clients and run reports using our booking app.
            Engage members using our community messaging feature.
          </p>
          <p className='md:text-xs-custom text-[1.35rem] mt-5 md:leading-3 sm:text-base sm:mt-5 md:mt-3 '>
            Be the first in line to take Oqulo for a test drive!
          </p>
          <div className='flex items-center gap-x-2 md:pt-2 md:mt-2 mt-10 pb-0 sm:flex-col sm:items-start sm:mt-5 sm:gap-y-4'>
            <Input />
            <Button title='NOTIFY ME' />
          </div>
          <p className='md:text-xs-custom text-base sm:text-xs md:mt-1 mt-3 sm:mt-5 text-slate-300 font-thin'>
            *No spam, that’s a promise.
          </p>
        </div>
      </div>
    </div>
  )
}

export default NavBar