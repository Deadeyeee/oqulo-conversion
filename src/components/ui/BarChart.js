import React from 'react'
import statistics from '../../assets/statistics.png'
import statisticsMobile from '../../assets/statistics-mobile.png'
import { motion } from 'framer-motion'
function BarChart() {

    return (
        <div className=' flex justify-center sm:w-full sm:h-96 rounded-[3rem] py-5 px-5 w-fit sm:shadow-none sm:p-0 shadow-[0px_15px_20px_rgba(0,0,0,0.1)]'>
            <motion.img initial={{ opacity: 0 }} whileInView={{ opacity: 1 }} transition={{ delay: 0.3, duration: .5 }} src={statistics} alt='statistics' className='sm:hidden' />
            <motion.img initial={{ opacity: 0 }} whileInView={{ opacity: 1 }} transition={{ delay: 0.3, duration: .5 }} src={statisticsMobile} alt='statistics mobile' className='hidden sm:flex' />
        </div>

    )
}

export default BarChart;