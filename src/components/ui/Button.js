import React from 'react'

export default function Button(props) {
  return (
    <button className='bg-gradient-to-b whitespace-nowrap shadow-3xl from-orange-light to-orange-dark md:text-xs-custom md:py-[0.0rem] md:px-5 rounded-full sm:text-base sm:py-3 sm:px-10 py-3 text-lg px-10'>{props.title}</button>
  )
}
