import React from 'react'

function Input() {
    return (
        <input type="email" className='shadow-3xl sm:w-full sm:text-base sm:py-2.5 sm:pl-8  text-black rounded-full md:text-xs-custom text-lg md:py-[0.00rem] md:w-10/12 w-9/12 md:pl-3 py-3 pl-5 ' placeholder='Email address'></input>
    )
}

export default Input