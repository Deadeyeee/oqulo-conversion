import React, { useRef } from 'react'
import NavBar from '../layouts/NavBar'
import clipArt from '../assets/clipArt.png'
import feature1 from '../assets/feature1.png'
import feature2 from '../assets/feature2.png'
import feature3 from '../assets/feature3.png'
import feature4 from '../assets/feature4.png'
import webui from '../assets/webui.png'
import { motion } from 'framer-motion'
import BarChart from '../components/ui/BarChart'
import Input from '../components/ui/Input'
import Button from '../components/ui/Button'
import Footer from '../layouts/Footer'

function LandingPage() {

  const discover = useRef(null);
  const feature = useRef(null);
  const contact = useRef(null);

  const scrollDiscover = () => discover.current.scrollIntoView({ behavior: 'smooth', block: 'start' });;
  const scrollFeature = () => feature.current.scrollIntoView({ behavior: 'smooth', block: 'start' });;
  const scrollContact = () => contact.current.scrollIntoView({ behavior: 'smooth', block: 'start' });;
  return (
    <div className='flex flex-col' ref={discover}>
      <NavBar
        discoverClick={scrollDiscover}
        featureClick={scrollFeature}
        contactClick={scrollContact}
      />

      <div className='flex justify-center w-full md:my-10 my-20 sm:my-[3rem]'>
        <div className='flex w-3/4 sm:w-11/12 justify-between md:gap-x-0 gap-x-24'>
          <div className='flex justify-center text-black flex-col md:w-full sm:w-full'>
            <h1 className='sm:text-lg md:text-xl text-4xl  font-semibold md:w-9/12 w-9/12'>
              Tried & Tested Space Management Software
            </h1>
            <div className='sm:flex sm:w-full hidden py-5 '>
              <motion.img initial={{ scale: 0 }} whileInView={{ scale: 1 }} transition={{ duration: .5 }} src={clipArt} className="w-full" alt="clipArt" />
            </div>
            <p className='md:text-xs-custom md:leading-3 sm:text-base md:mt-5 text-xl w-8/12 md:w-8/12 sm:w-full mt-10 sm:mt-5'>
              Oqulo is a homegrown app that’s been tested by real-life businesses. Whether you operate on a single building or in multiple locations, Oqulo is designed to make your space leasing operations hassle-free.
            </p>
            <p className='text-xl md:text-xs-custom md:leading-3 sm:text-base pt-5 md:w-8/12 w-8/12 sm:w-full'>
              Your clients will have a smooth booking & online payment experience, and your concierge staff will be able to view occupancy stats and generate reports at a click of a button.
            </p>
          </div>
          <div className='flex w-full sm:hidden'>
            <motion.img initial={{ scale: 0 }} whileInView={{ scale: 1 }} transition={{ duration: .5 }} src={clipArt} className="w-full" alt="clipArt" />
          </div>
        </div>
      </div>

      <div ref={feature} className='flex bg-nav-bg2 py-[5rem] justify-center h-[50rem] md:h-[21.5rem] md:py-[2.5rem] sm:h-[90rem] sm:py-[2rem]'>
        <div className='flex w-3/4 sm:w-11/12 justify-start items-center flex-col'>
          <h1 className='font-semibold text-4xl md:text-base sm:text-base sm:text-center sm:w-28'>Oqulo Features at a Glance</h1>
          <p className='text-xl md:text-xs-custom md:leading-3 sm:text-sm sm:text-center'>Powerful functionalities that changes the way you do business.</p>
          <div className='flex justify-center mt-16 md:mt-8 sm:mt-5 sm:flex-col'>
            <div className='flex flex-col w-1/3 gap-10 mt-16 sm:mt-0 md:mt-5 sm:w-full sm:order-1'>

              <motion.div initial={{ opacity: 0 }} whileInView={{ opacity: 1 }} transition={{ delay: 0.3, duration: .5 }} className='flex gap-10 md:gap-5'>
                <div className='w-4/6 sm:order-2'>
                  <h1 className='text-right font-semibold md:text-[.4rem] sm:text-left '>Powerful Space Management</h1>
                  <p className='text-right mt-5 md:text-[.38rem] md:mt-3 sm:text-left sm:text-sm'>Manage meeting room and desk bookings, create events, sell tickets, schedule private office showings, automate invoicing and connect with members all in one central dashboard.</p>
                </div>
                <div className='sm:order-1'>
                  <img src={feature1} className='md:w-5 sm:w-5' alt='feature 1' />
                </div>
              </motion.div>

              <motion.div initial={{ opacity: 0 }} whileInView={{ opacity: 1 }} transition={{ delay: 0.3, duration: .5 }} className='flex gap-10 md:gap-5'>
                <div className='w-4/6 sm:order-2'>
                  <h1 className='text-right font-semibold md:text-[.4rem] sm:text-left'>Painless Integration</h1>
                  <p className='text-right mt-5 md:text-[.38rem] md:mt-3 sm:text-left sm:text-sm'>No matter what your website is built on, Oqulo is easy to setup and integrate with CRM and payment gateways. Go live in a matter of days.</p>
                </div>
                <div className='sm:order-1'>
                  <img src={feature2} className='md:w-5 sm:w-6' alt='feature 2' />
                </div>
              </motion.div>

            </div>
            <motion.div initial={{ scale: 0 }} whileInView={{ scale: 1 }} transition={{ duration: .5, type: 'spring', bounce: .5 }} className='flex w-1/3  justify-center sm:order-3 sm:w-full sm:mt-10'>
              <img src={webui} style={{ filter: "drop-shadow(0px 0px 20px rgb(0,0,0, 0.3))" }} className='md:w-[10rem] w-[25rem] sm:w-11/12' alt='website ui' />
            </motion.div>
            <div className='flex flex-col gap-10 w-1/3 sm:order-2 sm:w-full'>
              <motion.div initial={{ opacity: 0 }} whileInView={{ opacity: 1 }} transition={{ delay: 0.3, duration: .5 }} className='flex gap-10 justify-end sm:justify-start mt-16  md:mt-5 md:gap-5'>
                <div>
                  <img src={feature3} className='md:w-5 sm:w-5' alt='feature 3' />
                </div>
                <div className='w-4/6'>
                  <h1 className='font-semibold md:text-[.4rem]'>Powerful Space Management</h1>
                  <p className='mt-5 md:text-[.38rem] md:mt-3 sm:text-left sm:text-sm'>Manage meeting room and desk bookings, create events, sell tickets, schedule private office showings, automate invoicing and connect with members all in one central dashboard.</p>
                </div>
              </motion.div>

              <motion.div initial={{ opacity: 0 }} whileInView={{ opacity: 1 }} transition={{ delay: 0.3, duration: .5 }} className='flex gap-10 justify-end sm:justify-start md:gap-5'>
                <div>
                  <img src={feature4} className='md:w-5 sm:w-5' alt='feature 4' />
                </div>
                <div className='w-4/6'>
                  <h1 className='font-semibold md:text-[.4rem]'>Painless Integration</h1>
                  <p className='mt-5 md:text-[.38rem] md:mt-3 sm:text-left sm:text-sm'>No matter what your website is built on, Oqulo is easy to setup and integrate with CRM and payment gateways. Go live in a matter of days.</p>
                </div>
              </motion.div>
            </div>
          </div>
        </div>
      </div>

      <div className='flex justify-center flex-col items-center w-full py-[5rem] '>
        <div className='flex justify-center flex-col items-center mt-32 md:mt-5 sm:mt-16'>
          <h1 className='font-semibold text-4xl md:text-lg text-center sm:text-2xl'>
            Stats Delivered Beautifully
          </h1>
          <p className='md:text-xs text-center'>
            View sales charts, booking ratio and user behavior using Oqulo's visual reporting feature.
          </p>
        </div>
        <div className='w-3/4 sm:w-full sm:mt-10 flex justify-center mt-10'>
          <BarChart />
        </div>
      </div>

      <div ref={contact} className='flex w-full bg-launch bg-cover bg-no-repeat py-20 justify-center sm:bg-gradient-to-br from-blue to-pink'>
        <div className='flex w-11/12 flex-col items-center justify-center text-white'>
          <h1 className=' text-4xl md:text-lg text-center sm:text-2xl tracking-wide'>Launching Soon</h1>
          <p className='font-light md:text-xs text-center mt-3'>sign up to get updates on Oqulo's public release</p>
          <div className='flex justify-center md:mt-5 md:gap-x-2 gap-x-5 w-5/12 mt-8 sm:flex-col sm:items-center sm:mt-5 sm:gap-y-4 sm:w-full'>
            <Input />
            <Button title='TRY THE BETA' />
          </div>
        </div>
      </div>

      <Footer
        discoverClick={scrollDiscover}
        featureClick={scrollFeature}
        contactClick={scrollContact}
      />
    </div>
  )
}

export default LandingPage