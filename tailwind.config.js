/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    // screens: {
    //   sm: '640px',
    //   md: '800px',
    //   lg: '1024px',
    //   xl: '1280px',
    //   '2xl': '1536',
    // },
    // screens: {
    //   sm: {'max': '640px'},
    //   md: {'max': '768px'},
    //   lg: {'max': '1024px'},
    //   xl: {'max': '1280px'},
    //   '2xl': {'max': '1536'},
    // },
    screens: {
      'sm': {'min': '1px', 'max': '767px'},
      // => @media (min-width: 640px and max-width: 767px) { ... }

      'md': {'min': '768px', 'max': '1023px'},
      // => @media (min-width: 768px and max-width: 1023px) { ... }

      'lg': {'min': '1024px', 'max': '1279px'},
      // => @media (min-width: 1024px and max-width: 1279px) { ... }

      'xl': {'min': '1280px', 'max': '1535px'},
      // => @media (min-width: 1280px and max-width: 1535px) { ... }

      '2xl': {'min': '1536px'},
      // => @media (min-width: 1536px) { ... }
    },
    
    extend: {
      backgroundImage: {
        'nav-bg': "url('assets/banner.png')",
        'nav-bg2': "url('assets/banner2.png')",
        'launch': "url('assets/background.png')",
        'footerBg': "url('assets/footerBg.png')",
      },
      colors: {
        blue: '#24459D',
        pink: '#C34C90',
        black: '#000000',
        'orange-light': 'rgba(239,122,76,1)',
        'orange-dark': 'rgba(224,96,95,1)',
      },
      height: {
        '311' : '311px',
        '490' : '58.50rem',
      },
      fontFamily:{
        'sans': ['Rubik', 'sans-serif'],
      },
      fontSize:{
        'xs-custom': '0.5rem',
        'xxs': '0.38rem',
      },
      boxShadow:{
        '3xl': '0px 5px 5px rgb(0,0,0,.2)'
      },
    },
  },
  plugins: [],
}